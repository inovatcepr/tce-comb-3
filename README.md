O projeto tce-comb integra uma série de micro serviços com o objetivo de 
proporocionar ao cidadão um serviço para controle do uso de veículos.

# Estrutura do código

O código é escrito para a JVM com Scala e Spring Boot. Os interesses são
separados em pacotes conforme abaixo (apenas algumas dependências externas
mostradas):

![](docs/tce-struct.png)

# Modelo de domínio

![](docs/tce-comb-3.png)

# API

Consulte no rodapé do programa.

# Rodando o código

É necessário baixar e instalar Java, Scala e SBT. Para rodar:

    $ sbt ~re-start
    
Ao final de poucos segundos, você deve ver algo parecido com:

    root 2017-05-28 17:16:50.329  INFO 178 --- [           main] pr.tce.comb.Application                  : Started Application in 3.867 seconds (JVM running for 4.168)

# Rodando a partir de um contêiner Docker

É necessário baixar e instalar o Docker. Depois:

    $ sbt docker:publishLocal

Ao final de poucos segundos, você deve ver algo como:

    [info] Successfully built 4b75bb9afc03
    [info] Successfully tagged tcepr/tce-comb-3:0.1.DEV
    [info] Built image tcepr/tce-comb-3:0.1.DEV
    [info] Update Latest from image tcepr/tce-comb-3:0.1.DEV
    [success] Total time: 27 s, completed 28/05/2017 17:31:42

A imagem tem cerca de 200MB apenas. Confira:

    $ docker images
    REPOSITORY                               TAG                 IMAGE ID            CREATED             SIZE
    tcepr/tce-comb-3                         0.1.DEV             4b75bb9afc03        3 minutes ago       196MB

Agora rode a imagem:

    $ docker run -p 8080:8080 tcepr/tce-comb-3:latest

Confira no navegador:

![](docs/tce-docker-test.png)

# Licença 

Todos os direitos são reservados ao Tribunal de Contas do Estado do Paraná.

