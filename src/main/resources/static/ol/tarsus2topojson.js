'use strict';

window.tarsus2topoJson = (function() {

    var replaces = [["\\],\\[0", "a"], ["\\],\\[1", "b"], ["\\],\\[-1", "c"], ["\\],\\[2", "d"], ["\\],\\[-2", "e"], ["\\],\\[3", "f"], ["\\],\\[-3", "g"], ["\\],\\[4", "h"], ["\\],\\[-4", "i"], ["\\],\\[5", "j"], ["\\],\\[-5", "k"], ["\\],\\[6", "l"], ["\\],\\[-6", "m"], ["\\],\\[7", "n"], ["\\],\\[-7", "o"], ["\\],\\[8", "p"], ["\\],\\[-8", "q"], ["\\],\\[9", "r"], ["\\],\\[-9", "s"], ["\\]\\],\\[\\[", "t"], ["\\]\\]", "u"], [",\\[\\[", "v"], ["\\[\\[", "x"], [",0", "A"], [",1", "B"], [",-1", "C"], [",2", "D"], [",-2", "E"], [",3", "F"], [",-3", "G"], [",4", "H"], [",-4", "I"], [",5", "J"], [",-5", "K"], [",6", "L"], [",-6", "M"], [",7", "N"], [",-7", "O"], [",8", "P"], [",-8", "Q"], [",9", "R"], [",-9", "S"]];

    function test() {
        var fs = require('fs');
        fs.readdir('tarsus/', function(err, list) {
            if (err) throw err;
            var count = 0;
            var loop = function() {
                if (list.length > 0) {
                    if (count < 10) {
                        count++;
                        var file = list.shift();
                        fs.readFile('tarsus/' + file, 'utf8', function(err, data) {
                            if (err) throw err;
                            console.log(file);
                            var topo = tarsus2TopoJson(data);
                            var err = fs.writeFileSync('topojson2/' + file, JSON.stringify(topo));
                            if (err) throw err;
                            count--;
                        });
                    } else {
                        setTimeout(loop, 200);
                    }
                } else {
                    conn.end();
                    callback(null);
                }
            };
            loop();
        });
    }

    function convertTarsus2TopoJson(tarsus) {
        var simpler = tarsus2Simpler(tarsus);
        var topoJson = simpler2TopoJson(simpler);
        return topoJson;
    }

    function simpler2TopoJson(simpler) {
        var objects = {};
        var estados = simpler[3];
        estados.forEach(function(estado) {
            var geometries = [];
            var cod = estado[0];
            var munics = estado[1];
            munics.forEach(function(munic) {
                var codMunic = munic[0];
                var arcs = munic[1];
                var type = typeof(arcs[0][0][0]) === 'undefined' ? 'Polygon' : 'MultiPolygon'; // testa se ÃƒÂ© um array
                var geometry = {
                    arcs: arcs,
                    type: type,
                    properties: {
                        cod: codMunic
                    }
                };
                geometries.push(geometry);
            });
            objects[cod] = {
                type: 'GeometryCollection',
                geometries: geometries
            };
        });

        var topo = {
            type: 'Topology',
            transform: {
                scale: simpler[0],
                translate: simpler[1]
            },
            arcs: simpler[2],
            objects: objects,
            bbox: simpler[4]
        };

        return topo;
    }

    function tarsus2Simpler(tarsus) {
        var myStr = tarsus;
//        replaces.sort(function(s1, s2) {
//            return s1.length - s2.length;
//        });
        replaces.reverse().forEach(function(rep) {
           myStr = myStr.replace(new RegExp(rep[1], 'g'), rep[0].replace(/\\/g, ''));
        });

//        var fs = require('fs');
//        fs.writeFileSync('topojson2/simpler.json', myStr);

        var simpler = JSON.parse(myStr);

        return simpler;
    }

    return convertTarsus2TopoJson;

})();
