
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">


    <!-- Arquivos externos para o mapa -->

    <!-- <link rel="stylesheet" href="ol/ol.css" type="text/css">--> <!-- http://openlayers.org/en/v3.4.0/css/ol.css -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/ol3/3.4.0/ol.min.css" type="text/css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script src="ol/tarsus2topojson.js" type="text/javascript"></script>

    <!-- <script src="ol/ol.js" type="text/javascript"></script> -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/ol3/3.4.0/ol.js" type="text/javascript"></script>
    <!-- Após testes, verificou-se que o arquivo do OL3 não minificado era menor que o minificado -->

    <script src="lista_municipios_extras.js" type="text/javascript"></script>

    <style type="text/css">
	html, body, #mapdiv{
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 0;
		cursor: pointer;
	}
	#mapdiv{
		position: absolute;
		z-index: 1;
		top: 0;
		left: 0;
	}
	#tooltip{
		position: absolute;
		z-index: 1000;
		right: 8px;
		top: 8px;
		color: #FFF;
		background-color: #4a74a8;
		height: auto;
		width: auto;
		padding: 3px;
		font-family: Arial;
		font-size: 12px;
		border-radius: 2px;
		border: 3px solid #FFF;
		opacity: 0.7;
		display: none;
	}

</style>

</head>
<body>

<div id='tooltip'></div>
<div id='mapdiv'></div>


<script type="text/javascript">






var capitalCod = '';

var localcod = window.location.toString();
	if(localcod != ''){
		localcod = localcod.split('codigo=');
		if(localcod.length > 1){
			localcod = localcod[1];
			localcod = localcod.split('&');
			localcod = localcod[0];
		}
	}
	if(typeof localcod != 'string'){
		localcod = '';
	}else{

		//Carrega arquivo de nomes
		var lista_mun_js_file = localcod.substr(0,2);
		(function() {
		    var d=document,
		    h=d.getElementsByTagName('head')[0],
		    s=d.createElement('script');
		    s.type='text/javascript';
		    s.src='lista_municipios_'+lista_mun_js_file+'.min.js';
		    h.appendChild(s);
		})();

		//Pega o código da capital
		capitalCod = DicUFsCapitais[localcod];

		//Carrega malhas

		function MapaService() {
		    var _tarsus2topoJson = window.tarsus2topoJson;

		    console.log($);

		    $.support.cors = true;

		    var subCod = '';
		    	if(localcod.length >= 2){
		    		subCod = localcod.substr(0,2);
		    	}

		    var urlMapas = "http://servicomapas.ibge.gov.br/api/mapas/" + subCod + '/1'; //O "1" significa 1 nívelabaixo do que érequisitado. UF quebrado
		    $.getJSON(urlMapas, function(mapa, status, xmlHttpRequest) {
				var topojson = _tarsus2topoJson(mapa.Tarsus);
				//console.log(topojson);
			    CriaMapa(topojson);
		    });
		}





		MapaService();


		//Cria mapa

		function CriaMapa(topoj){
			var _view = new ol.View({
            	center: ol.proj.transform([0,0], 'EPSG:4326', 'EPSG:3857'),
            	zoom: 5
        	});

        	var _style = [new ol.style.Style({
                fill: new ol.style.Fill({
                    //color: 'rgba(125, 202, 38, 0.6)'//'#7DCA26' //rgba(255, 255, 255, 0.2)' //Cor das capitais
                    color: 'rgba(255, 102, 0, 0.6)'//'#FF6600' //rgba(255, 255, 255, 0.2)' //Cor dos municípios não capitais
                }),
                stroke: new ol.style.Stroke({
                    color: 'rgb(255, 255, 153)',//'#FFFF99',//'rgb(125, 125, 125)',
                    width: 1//0.5
                })
            })];

            var _capStyle = [new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(125, 202, 38, 0.6)'//'#7DCA26' //rgba(255, 255, 255, 0.2)' //Cor das capitais
                    //color: 'rgba(255, 102, 0, 0.6)'//'#FF6600' //rgba(255, 255, 255, 0.2)' //Cor dos municípios não capitais
                }),
                stroke: new ol.style.Stroke({
                    color: 'rgb(255, 255, 153)',//'#FFFF99',//'rgb(125, 125, 125)',
                    width: 1//0.5
                })
            })];

            var funcStyle = function(feature, resolution){
            	if(feature['n']['cod'] == capitalCod){
            		console.log('ok');
            		return _capStyle;
        		}
        		return _style;
        	}

        	var _layer = new ol.layer.Vector({
                source: new ol.source.TopoJSON({
                    projection: 'EPSG:3857',
                    object: topoj
                }),
                style: funcStyle/*(function(){
                	return funcStyle(feature,resolution);
                })(feature,resolution)
				*/
                //funcStyle //_style
            });

        	var _map = new ol.Map({
        		target: 'mapdiv',
        		layers: [
          			new ol.layer.Tile({
            			//source: new ol.source.MapQuest({layer: 'sat'})
              			source: new ol.source.OSM()
          			}),
          			_layer
        		],
        		view: _view
        	});

        	var _extent = _layer.getSource().getExtent();
        	_map.getView().fitExtent(_extent, _map.getSize());

        	//Clique
        	_map.on("click", function(e) {
    			_map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
        			var clickCod = feature['n']['cod'];
        			window.parent.location = '/api/city/data?cityId='+clickCod;
			    });
			});

			//Hover
        	_map.on("pointermove", function(e) {
    			_map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
        			var clickCod = feature['n']['cod'];
        			var clickName = DicMun[clickCod.toString().substr(0,2)][clickCod.toString()];
        			document.getElementById('tooltip').innerHTML = clickName;
        			if(document.getElementById('tooltip').style.display != 'block'){
        				document.getElementById('tooltip').style.display = 'block';
        			}
        			var t = setTimeout('unshowTooltip("'+clickName+'")',2000);
			    });
			});


		}


}//Fim do else, que checa a existencia do localcod

function unshowTooltip(n){
	if(n == document.getElementById('tooltip').innerHTML){
		document.getElementById('tooltip').style.display = 'none';
	}
}

</script>

</body>
</html>