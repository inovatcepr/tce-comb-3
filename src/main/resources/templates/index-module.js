    //
    //

	coords.then(function(result) {
	    $scope.searchString = "Curitiba"; // TODO resolve
	});

    $scope.searchString = "CURITIBA";

    $scope.year = 2017;

    // Lista as cidades
    // (executado ao entrar)

    $ctrl.listCities = function(searchString, reset) {
        $log.info("Listing cities like " + searchString);
        $http.post("/api/city/data/search", {nmMunicipio: searchString})
        .then(function(response) {
            $scope.cityList = response.data;
            $scope.searchString = "";
            if (response.data && reset) { $scope.city = response.data[0]; };
        }).catch(function(e) { $log.error("POST /api/city/data/search FAILED"+e); })
    };


    // controla o deslocamento de municípios no gráfico

    // City data

    $ctrl.getCity = function(searchString) {
        $log.info("Getting city like " + searchString);
        $http.post("/api/city/data/search", {nmMunicipio: searchString})
        .then(function(response) {
            if (response.data) { $scope.city = response.data[0]; };
        }).catch(function(e) { $log.error("POST /api/city/data/search FAILED"+e); })
    };

    // quantidade de cidades a mostrar

    $scope.showingCities = 10;

//    $ctrl.setShowingCities = function(value) { $scope.showingCities = value; }

    // Intial actions

    $ctrl.listCities("", false);
    $ctrl.getCity($scope.searchString);

    $scope.$watch("city", function(city) {
        if (city) { $ctrl.listEntityData(city); }
    });

    $ctrl.listEntityData = function(city) {
        $log.info("Listing entities data for " + city);
        $http.get("/api/entity/data/list", {params: {cityId: city._id}})
        .then(function(response) {
            $scope.entityDataList = response.data;
            city.entityDataCount = response.data.length;
            if (response.data) {
                $ctrl.setEntityData(response.data[0]);
            };
        }).catch(function(e) { $log.error("GET /api/entity/data/list FAILED"+e); })
    };

    $ctrl.setEntityData = function(value) {
        $scope.entityData = value;
    }

    $scope.$watch("entityData", function(value) {
        if (value) {
            $ctrl.aggregateUsages(value._id);
        }
    });

    $ctrl.aggregateUsages = function(entityId) {
        $http.get("/api/quantity/entity", {params: {entityId: entityId, year: $scope.year}})
        .then(function(response) {
            $scope.usageAggregate = response.data;
        }).catch(function(e) { $log.error("GET /api/quantity/entity FAILED"+e); })
        $http.get("/api/price/entity", {params: {entityId: entityId, year: $scope.year}})
        .then(function(response) {
            $scope.priceAggregate = response.data;
        }).catch(function(e) { $log.error("GET /api/price/aggregate FAILED"+e); })
    }

    // totaliza despesa por cidade

    $ctrl.aggregateCities = function(cityId) {
        $http.get("/api/quantity/city", {params: {cityId: entityId, year: $scope.year}})
        .then(function(response) {
            $scope.cityAggregate = response.data;
        }).catch(function(e) { $log.error("GET /api/quantity/city FAILED"+e); })
    }


    // Exclusões
    //

    $http.get("/api/city/data/exclusion", {params: {year: $scope.year}})
    .then(function(response) {
        $scope.exclusions = response.data;
    }).catch(function(e) { $log.error("GET /api/city/data/exclusion FAILED"+e); })

    // Equipe
    //

    $http.get("/api/team/dev")
    .then(function(response) {
        $scope.developers = response.data;
    }).catch(function(e) { $log.error("GET /api/team/dev FAILED"+e); })

    $http.get("/api/team/sponsor")
    .then(function(response) {
        $scope.sponsors = response.data;
    }).catch(function(e) { $log.error("GET /api/team/sponsor FAILED"+e); })

    // Setup
    //

    $ctrl.doSetup = function() {
        $scope.setupMessage = "Aguarde...";
        $http.post("/api/setup")
        .then(function(response) {
            $scope.setupMessage = "Setup concluído com sucesso.";
        }, function() {
            $scope.setupMessage = "O setup não pode ser concluído.";
        }).catch(function(e) { $log.error("POST /api/setup FAILED"+e); })
    }


