<aside class="sidebar">
    <div class="sidebar-container">
        <div class="sidebar-header">
            <div class="brand">
                <div class="logo">  <img src="logo/tce-logo-32.png" style="vertical-align: top;"/> </div>
                TCE/PR
            </div>
        </div>
        <nav class="menu">
            <ul class="nav metismenu" id="sidebar-menu" style="display: block;">
                <li ng-class="{active: '${mainPage!'index-00'}'=='index-00'}">
                    <a href="/"> <i class="fa fa-home"></i> Comparativo </a>
                </li>
                <li ng-class="{active: '${mainPage!'index-01'}'=='index-01'}">
                    <a href="/city"> <i class="fa fa-building-o"></i> Sua Cidade </a>
                </li>
                <li ng-class="{active: '${mainPage!'index-02'}'=='index-02'}">
                    <a href="/vehicle"> <i class="fa fa-car"></i> Veículos </a>
                </li>
            </ul>
        </nav>
    </div>
    <footer class="sidebar-footer">
        <!--<ul class="nav metismenu" id="customize-menu">-->
            <!--<li>-->
                <!--<a href="/settings"> <i class="fa fa-cog"></i> Ajustes </a>-->
            <!--</li>-->
        <!--</ul>-->
    </footer>
</aside>
