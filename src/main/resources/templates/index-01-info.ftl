<div class="card sameheight-item stats" data-exclude="xs">
    <div class="card-header bordered">
        <div class="header-block">
            <h3 class="title"> Infográfico </h3>
        </div>
    </div>
    <div class="card-body">
        <div class="title-block">
            <p class="title-description"> Veja abaixo os dados disponíveis para esta entidade:</p>
        </div>
        <div class="row row-sm stats-container">
            <div class="col-xs-12 col-sm-6 stat-col">
                <div class="stat-icon"> <i class="fa fa-map-marker"></i> </div>
                <div class="stat">
                    <div class="value"> {{ info.entityDataCount }} </div>
                    <div class="name"> Número de entidades </div>
                </div>
                <progress class="progress stat-progress" value="100" max="100">
                    <div class="progress">
                        <span class="progress-bar" style="width: 100%;"></span>
                    </div>
                </progress>
            </div>
            <div class="col-xs-12 col-sm-6  stat-col">
                <div class="stat-icon"> <i class="fa fa-dollar"></i> </div>
                <div class="stat">
                    <div class="value"> $ {{ cityAggregate.total | number:0 }} </div>
                    <div class="name"> Gasto anual </div>
                </div>
                <progress class="progress stat-progress" value="100" max="100">
                    <div class="progress">
                        <span class="progress-bar" style="width: 100%;"></span>
                    </div>
                </progress>
            </div>
            <div class="col-xs-12 col-sm-6  stat-col">
                <div class="stat-icon"> <i class="fa fa-map-o"></i> </div>
                <div class="stat">
                    <div class="value"> {{ cityAggregate.area | number:2 }} </div>
                    <div class="name"> Área (km2)</div>
                </div>
                <progress class="progress stat-progress" value="100" max="100">
                    <div class="progress">
                        <span class="progress-bar" style="width: 100%;"></span>
                    </div>
                </progress>
            </div>
            <div class="col-xs-12 col-sm-6 stat-col">
                <div class="stat-icon"> <i class="fa fa-users"></i> </div>
                <div class="stat">
                    <div class="value"> {{ cityAggregate.populacao }} </div>
                    <div class="name"> Número de habitantes </div>
                </div>
                <progress class="progress stat-progress" value="100" max="100">
                    <div class="progress">
                        <span class="progress-bar" style="width: 100%;"></span>
                    </div>
                </progress>
            </div>
        </div>
    </div>
</div>

