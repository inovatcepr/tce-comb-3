<div class="card sameheight-item sales-breakdown" data-exclude="xs,sm,lg">
    <div class="card-header">
        <div class="header-block">
            <h3 class="title"> Gastos com combustível </h3>
        </div>
    </div>
    <div class="card-body">
        <div google-chart chart="fuelPieChart"></div>
    </div>
</div>