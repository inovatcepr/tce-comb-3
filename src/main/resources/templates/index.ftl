<!DOCTYPE html>
<html class="no-js" lang="en" ng-app="appIndex" >
<head>
    <title>Uso de veículos</title>
    <link rel="stylesheet" id="theme-style" href="css/app-blue.css">
    <link rel="stylesheet" id="theme-style" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="/webjars/bootstrap/4.0.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/webjars/angular-ui-bootstrap-bower/2.5.0/ui-bootstrap-csp.css">
    <link rel="stylesheet" href="/webjars/tether/1.4.3/dist/css/tether.min.css">
    <link rel="stylesheet" href="/webjars/metisMenu/2.5.0/dist/metisMenu.min.css">
    <script src="/webjars/jquery/2.2.4/dist/jquery.min.js"></script>
</head>
<body ng-controller="IndexController as $ctrl">
<div class="main-wrapper">
    <div class="app" id="app">
        <#include "_header.ftl" />
        <#include "_sidebar.ftl" />
        <div class="sidebar-overlay" id="sidebar-overlay"></div>
        <#include "${mainPage!'index-00'}.html" />
        <#include "_footer.html" />
    </div>
</div>
<!-- Reference block for JS -->
<div class="ref" id="ref">
    <div class="color-primary"></div>
    <div class="chart">
        <div class="color-primary"></div>
        <div class="color-secondary"></div>
    </div>
</div>
<script>
            (function(i, s, o, g, r, a, m)
            {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function()
                {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-80463319-2', 'auto');
            ga('send', 'pageview');
        </script>
<script src="/webjars/popper.js/1.12.9/dist/popper.min.js"></script>
<script src="/webjars/tether/1.4.3/dist/js/tether.min.js"></script>
<script src="/webjars/angular/1.6.9/angular.min.js"></script>
<script src="/webjars/angular-google-chart/0.1.0/ng-google-chart.min.js"></script>
<script src="/webjars/angular-ui-bootstrap-bower/2.5.0/ui-bootstrap-tpls.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/dist/js/bootstrap.min.js"></script>
<script src="/webjars/metisMenu/2.5.0/dist/metisMenu.min.js"></script>
<script src="js/app.js"></script>
<script>
angular.module("appIndex", ['googlechart', 'ui.bootstrap'])
.service("coords", ["$q", "$http", function($q, $http) {

    var deferred = $q.defer();
    $http.get("http://ip-api.com/json")
    .then(function(coordinates) {
       var myCoordinates = {};
       myCoordinates.city = coordinates.city;
       myCoordinates.city = coordinates.city;
       deferred.resolve(myCoordinates);
    })
    return deferred.promise;

}])
.controller("IndexController", ["$rootScope", "$scope", "$http", "coords", "$log"
, function($rootScope, $scope, $http, coords, $log) {

  var $ctrl = this;
  <#include "index-module.js"/>

}])
<#include "index-directive.js" />
;
</script>
</body>
</html>
