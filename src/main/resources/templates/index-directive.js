// Diretiva para apresentação dos gráficos de barra

.directive("usageChart", ["$http", "$log", function($http, $log) {
    return {
        restrict: 'A',
        transclude: true,
        scope: { usageChart: '=', showing: '=' },
        link: function(scope, element, attrs) {

            // Movimentação dos municípios nos gráficos
            // O usuário pode movimentar mais à esquerda ou mais à direita

            scope.leftShift = 0;
            scope.rightShift = 0;

            scope.moreCities = function() {
                $("#modal-compare").modal('show');
            }

            scope.recalcLeft = function(sign) {
                if      (sign == 1  && scope.leftShift<50) { scope.leftShift++; }
                else if (sign == -1 && scope.leftShift>0)  { scope.leftShift--; }
                scope.renderLeftChart(scope.usageChart, scope.showing);
            }
            scope.recalcRight = function(sign) {
                if      (sign == 1  && scope.rightShift<50) { scope.rightShift++; }
                else if (sign == -1 && scope.rightShift>0)  { scope.rightShift--; }
                scope.renderRightChart(scope.usageChart, scope.showing);
            }

            // exibição dos gráficos

            scope.renderLeftChart = function(index, slice) {
                var leftRequest = {chart: index, slice: slice, skip: scope.leftShift, ordered: "ASC", color: "#74828f" };
                $http.post("/api/expenditure/chart/bar", leftRequest)
                .then(function(response) {
                    scope.leftChart = response.data;
                    scope.waiting = false;
                }).catch(function(e) { $log.error("POST /api/expenditure/chart/bar FAILED (left)"+e); })
            }
            scope.renderRightChart = function(index, slice) {
                var rightRequest = {chart: index, slice: slice, skip: scope.rightShift, ordered: "DESC", color: "#c25b56" };
                $http.post("/api/expenditure/chart/bar", rightRequest)
                .then(function(response) {
                    scope.rightChart = response.data;
                    scope.waiting = false;
                }).catch(function(e) { $log.error("POST /api/expenditure/chart/bar FAILED (right)"+e); })
            }

            // call-back necessário para atualizar os gráficos

            scope.$watch('[usageChart, showing]', function(pair) {
                $log.info("Rendering "+pair)
                if (pair) {
                    scope.waiting = true;
                    $log.info("Rendering chart "+pair[0])
                    scope.renderLeftChart(pair[0], pair[1]);
                    scope.renderRightChart(pair[0], pair[1]);
                }
            });
        },
        templateUrl: '/template/index-00-chart'
    };
}])

// Diretiva para apresentação do gráfico de pizza

.directive("fuelChart", ["$http", "$log", function($http, $log) {
    return {
        restrict: 'A',
        transclude: true,
        scope: { fuelChart: '=', year: '=' },
        link: function(scope, element, attrs) {

            // exibição do gráfico

            scope.renderChart = function(entityId) {
                // TODO substituir abaixo {params: {entityId: entityId, year: scope.year}}
                $http.post("/api/expenditure/chart/pie", null, {params: {entityId: entityId}})
                .then(function(response) {
                    scope.fuelPieChart = response.data;
                    scope.waiting = false;
                }).catch(function(e) { $log.error("POST /api/expenditure/chart/pie FAILED"+e); })
            }

            // call-back necessário para atualizar o gráfico

            scope.$watch('fuelChart', function(entityId) {
                scope.waiting = true;
                $log.info("Rendering fuel chart "+entityId)
                scope.renderChart(entityId);
            });
        },
        templateUrl: '/template/index-01-fuel'
    };
}])

// Diretiva para o infográfico

.directive("info", ["$http", "$log", function($http, $log) {
    return {
        restrict: 'A',
        transclude: true,
        scope: { info: '=', year: '=' },
        link: function(scope, element, attrs) {

            // conta veículos
            // estima totais de preços

            scope.getInfo = function(city) {
                $http.get("/api/vehicle/count", {params: {cityId: city._id}})
                .then(function(response) {
                    scope.vehicleCount = response.data;
                }).catch(function(e) { $log.error("GET /api/vehicle/count FAILED"+e); })
                $http.get("/api/quantity/city", {params: {cityId: city._id, year: scope.year}})
                .then(function(response) {
                    scope.cityAggregate = response.data;
                }).catch(function(e) { $log.error("GET /api/quantity/city FAILED"+e); })
            }

            // call-back necessário para atualizar o infográfico

            scope.$watch('info', function(city) {
                scope.waiting = true;
                $log.info("Rendering info "+city)
                scope.getInfo(city);
            });
        },
        templateUrl: '/template/index-01-info'
    };
}])


// Diretiva para o perfil da equipe

.directive("developerProfile", function() {
    return {
        restrict: 'A',
        scope: { developerProfile: '=' },
        templateUrl: '/template/index-92-developer'
    };
})

// Diretiva para o perfil dos patrocinadores

.directive("sponsorProfile", function() {
    return {
        restrict: 'A',
        scope: { sponsorProfile: '=' },
        templateUrl: '/template/index-92-sponsor'
    };
})
