<div class="row">
    <div class="col-md-3">
        <img ng-src="{{sponsorProfile.imageUrl}}" style="max-width: 180px;">
    </div>
    <div class="col-md-9">
        <div class="card-body">
            <h4 class="card-title">{{ sponsorProfile.fullName }}</h4>
            <div class="card-text">
                <p>{{ sponsorProfile.role }}</p>
            </div>
            <p class="card-text">{{ sponsorProfile.testimonial }}</p>
            <hr/>
        </div>
    </div>
</div>
