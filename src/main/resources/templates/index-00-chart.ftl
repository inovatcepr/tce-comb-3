<div class="card-header" style="background-color: white;">
    <div class="header-block">
        <h3 class="title"><ng-transclude></ng-transclude></h3>
    </div>
</div>
<div class="card-body">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <span class="float-sm-right">
                <div class="btn-group btn-group-sm" role="group" aria-label="Mover">
                    <a class="btn btn-secondary" ng-click="recalcLeft(-1)"><i class="fa fa-chevron-down"></i></a>
                    <a class="btn btn-secondary" ng-click="recalcLeft( 1)"><i class="fa fa-chevron-up"></i></a>
                </div>
            </span>
            <p class="size-h4 text-info">Menor despesa</p>
            <p class="text-secondary">
                <em>Mostrando <span ng-show="leftShift >0">de {{ leftShift + 1 }} a </span>{{ leftShift + 5 }}<span ng-hide="leftShift <50" > (máx.)</span></em>
            </p>
            <div ng-hide="waiting" google-chart chart="leftChart"></div>
            <div ng-show="waiting">Aguarde...</div>
        </div>
        <div class="col-xs-12 col-md-6">
            <span class="float-sm-right">
                <div class="btn-group btn-group-sm" role="group" aria-label="Mover">
                    <a class="btn btn-secondary" ng-click="recalcRight(-1)"><i class="fa fa-chevron-down"></i></a>
                    <a class="btn btn-secondary" ng-click="recalcRight( 1)"><i class="fa fa-chevron-up"></i></a>
                </div>
            </span>
            <p class="size-h4 text-info">Maior despesa</p>
            <p class="text-secondary">
                <em>Mostrando <span ng-show="rightShift > 0">de {{ rightShift + 1 }} a </span>{{ rightShift + 5 }}<span ng-hide="rightShift <50" > (máx.)</span></em>
            </p>
            <div ng-hide="waiting" google-chart chart="rightChart"></div>
            <div ng-show="waiting">Aguarde...</div>
        </div>
    </div>
</div>