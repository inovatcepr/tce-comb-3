<div class="row">
    <div class="col-md-3">
        <img ng-src="{{developerProfile.imageUrl}}" style="max-width: 180px;">
    </div>
    <div class="col-md-9">
        <div class="card-body">
            <h4 class="float-sm-right">
                <a ng-show="developerProfile.facebookProfile!=''" href="{{ developerProfile.facebookProfile }}" target="_profile">
                    <i class="fa fa-facebook-square" aria-hidden="true"></i>
                </a>
                <a ng-show="developerProfile.linkedInProfile!=''" href="{{ developerProfile.linkedInProfile }}" target="_profile">
                    <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                </a>
                <a ng-show="developerProfile.twitterProfile!=''" href="{{ developerProfile.twitterProfile }}" target="_profile">
                    <i class="fa fa-twitter-square" aria-hidden="true"></i>
                </a>
                <a ng-show="developerProfile.gitHubProfile!=''" href="{{ developerProfile.gitHubProfile }}" target="_profile">
                    <i class="fa fa-github-square" aria-hidden="true"></i>
                </a>
            </h4>
            <h4 class="card-title">{{ developerProfile.fullName }}</h4>
            <div class="card-text">
                <a ng-show="developerProfile.companyLogoUrl!=''" href="{{ developerProfile.companySiteUrl }}" target="_profile">
                    <img class="float-sm-right" ng-src="{{developerProfile.companyLogoUrl}}" style="max-width: 180px;">
                </a>
                <a href="{{ developerProfile.companySiteUrl }}" target="_profile">
                    <span>{{ developerProfile.companyName }}</span>
                </a>
            </div>
            <p class="card-text">{{ developerProfile.testimonial }}</p>
            <hr/>
        </div>
    </div>
</div>
