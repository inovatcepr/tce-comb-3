package pr.tce.comb

import org.springframework.data.annotation.Id

/**
  * Dados de um veículo.
  *
  * Created by mauriciofernandesdecastro on 21/05/17.
  */
class VehicleData(

  @Id
  var _id: String = "",

  var cdBem: String = "",

  var fuel: String = ""

)