package pr.tce.comb.controller

import io.swagger.annotations.ApiOperation
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation._
import pr.tce.comb.{UserComment, Vehicle}
import pr.tce.comb.service.{CityDataService, UserCommentService}
import pr.tce.comb.util.AuthorityExtractor

/**
  * Comentários de usuários.
  *
  * Created by mauriciofernandesdecastro on 27/05/17.
  */
@RestController
@RequestMapping(Array("/api/user/comment"))
@PreAuthorize("hasRole('ROLE_USER')")
class UserCommentController (service: UserCommentService) extends AuthorityExtractor {

  /**
    * GET /api/user/comment/list?entityId
    */
  @ApiOperation("Lista comentários dado um caminho.")
  @GetMapping(path = Array("/list"))
  def list(@RequestParam commentPath: String) =
  service.list(commentPath)

  /**
    * GET /api/user/comment?id
    */
  @ApiOperation("Recupera um comentário.")
  @GetMapping
  def get(@RequestParam id: String) =
  service.get(id)

  /**
    * POST /api/user/comment?commentPath
    */
  @ApiOperation("Novo comentário.")
  @PostMapping
  def create(implicit principal: Authentication, @RequestBody commentPath: String) =
  new UserComment(identityId = _identityId, commentPath = commentPath)

  /**
    * PUT /api/user/comment
    */
  @ApiOperation("Salva ou atualiza um comentário.")
  @PutMapping
  def saveOrUpdate(implicit principal: Authentication, @RequestBody command: UserComment) =
  service.saveOrUpdate(_identityId, command)

}
