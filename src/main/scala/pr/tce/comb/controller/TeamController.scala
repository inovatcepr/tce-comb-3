package pr.tce.comb.controller

import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation._
import pr.tce.comb.service.TeamService

/**
  * Mostra o perfil do desenvolvedor.
  *
  * Created by mauriciofernandesdecastro on 24/06/17.
  */
@RestController
@RequestMapping(Array("/api/team/"))
class TeamController(service: TeamService) {

  /**
    * GET /api/team/{teamType}
    */
  @ApiOperation("Lista os envolvidos de acordo com o tipo: dev | sponsor ")
  @GetMapping(path = Array("/{teamType}"))
  def load(@PathVariable teamType: String) = service.loadTeam(teamType)

}
