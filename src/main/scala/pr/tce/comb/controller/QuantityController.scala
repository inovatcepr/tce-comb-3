package pr.tce.comb.controller

import io.swagger.annotations.ApiOperation
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation._
import pr.tce.comb.Usage
import pr.tce.comb.service.QuantityService
import pr.tce.comb.util.AuthorityExtractor

/**
  * Controlador de uso de veículos.
  *
  * @param service
  */
@RestController
@RequestMapping(Array("/api/quantity"))
class QuantityController(service: QuantityService) extends AuthorityExtractor {

  /**
    * GET /api/quantity/entity?entityId&year
    */
  @ApiOperation("Agrega usos (consumo de combustível) dos veículos de uma entidade")
  @GetMapping(path = Array("/entity"))
  def aggregate(@RequestParam entityId: String, @RequestParam year: Int) =
  service.aggregate(entityId, year)

  /**
    * GET /api/quantity/city?entityId&year
    */
  @ApiOperation("Agrega consumo de combustível dos veículos de uma cidade")
  @GetMapping(path = Array("/city"))
  def byCity(@RequestParam cityId: String, @RequestParam year: Int) =
  service.byCity(cityId, year)

  /**
    * GET /api/quantity/list?vehicleId&year
    */
  @ApiOperation("Lista consumo de um veículo")
  @GetMapping(path = Array("/list"))
  def list(@RequestParam vehicleId: String, @RequestParam year: Int) =
  service.list(vehicleId)

  /**
    * GET /api/quantity?id
    */
  @ApiOperation("Recupera um registro de consumo")
  @GetMapping
  def getOrCreate(@RequestParam id: String) =
  service.get(id)

  /**
    * PUT /api/quantity
    */
  @ApiOperation("Salva ou atualiza uso")
  @PutMapping
  @PreAuthorize("hasRole('ROLE_WRITE')")
  def saveOrUpdate(implicit principal: Authentication, @RequestBody command: Usage) =
  service.saveOrUpdate(_identityId, command)

}
