package pr.tce.comb.controller

import io.swagger.annotations.ApiOperation
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation._
import pr.tce.comb.service.HomeService

/**
  * Serve páginas como 'SPA' (Single Page Aplication).
  *
  * O uso desse controlador dispensa 'router' do lado do cliente.
  */
@Controller
@RequestMapping(Array("/"))
class RouterController(service: HomeService) {

  @GetMapping
  @ApiOperation("Página inicial.")
  def home(model: Model) = select(model, "00")

  @GetMapping(path = Array("/template/{fileName}"))
  @ApiOperation("Facilita o uso de templates para diretivas AngularJs.")
  def template(@PathVariable fileName: String) = s"/$fileName"

  @GetMapping(path = Array("/city"))
  @ApiOperation("Página de cidades.")
  def city(model: Model) = select(model, "01")

  @GetMapping(path = Array("/vehicle"))
  @ApiOperation("Página de veículos.")
  def vehicle(model: Model) = select(model, "02")

  // TODO implementar páginas de usuário, colaboração, perfil e notificações

//  @GetMapping(path = Array("/user"))
//  @ApiOperation("Página de usuários.")
//  def user(model: Model) = select(model, "03")
//
//  @GetMapping(path = Array("/collab"))
//  @ApiOperation("Página de colaboração.")
//  def collab(model: Model) = select(model, "04")
//

  @GetMapping(path = Array("/map"))
  @ApiOperation("Mapa.")
  def map(model: Model) = "map"

//  @GetMapping(path = Array("/profile"))
//  @ApiOperation("Página de perfil.")
//  def profile(model: Model) = select(model, "90")
//
//  @GetMapping(path = Array("/notification"))
//  @ApiOperation("Página de notificações.")
//  def notify(model: Model) = select(model, "91")

  @GetMapping(path = Array("/team"))
  @ApiOperation("Página de equipe.")
  def team(model: Model) = select(model, "92")

  @GetMapping(path = Array("/settings"))
  @ApiOperation("Página de ajustes.")
  def settings(model: Model) = select(model, "99")

  private def select(model: Model, page: String = "00") = {
    model.addAttribute("mainPage", s"index-$page")
    s"index"
  }

}
