package pr.tce.comb.controller

import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation._
import pr.tce.comb.service.{ChartRequest, ExpenditureService}

/**
  * Quantifica despesas.
  *
  * Created by mauriciofernandesdecastro on 24/06/17.
  */
@RestController
@RequestMapping(Array("/api/expenditure"))
class ExpenditureController(service: ExpenditureService) {

  /**
    * GET /api/expenditure/city?slice&skip&ordered
    */
  @ApiOperation("Despesas de uma cidade")
  @GetMapping(path = Array("/city"))
  def city(@RequestParam slice: Int, @RequestParam skip: Int, @RequestParam ordered: String) =
    service.total(slice, skip, ordered, "expenditure")

  /**
    * POST /api/expenditure/chart/bar
    */
  @ApiOperation("Formata um gráfico de acordo com tipos")
  @PostMapping(path = Array("/chart/bar"))
  def chartBar(@RequestBody chartRequest: ChartRequest) =
  service.chartBar(chartRequest)

  /**
    * POST /api/expenditure/chart/pie
    */
  @ApiOperation("Formata um gráfico de pizza")
  @PostMapping(path = Array("/chart/pie"))
  def chartPie(entityId: String) =
  service.chartPie(entityId)

}