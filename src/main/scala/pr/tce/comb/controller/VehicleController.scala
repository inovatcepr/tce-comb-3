package pr.tce.comb.controller

import io.swagger.annotations.ApiOperation
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation._
import pr.tce.comb.Vehicle
import pr.tce.comb.service.VehicleService
import pr.tce.comb.util.AuthorityExtractor

@RestController
@RequestMapping(Array("/api/vehicle"))
class VehicleController(service: VehicleService) extends AuthorityExtractor {

  /**
    * GET /api/vehicle/list?entityId
    */
  @ApiOperation("Lista veículos de uma entidade")
  @GetMapping(path = Array("/list"))
  def list(@RequestParam entityId: String) =
  service.list(entityId)

  /**
    * GET /api/vehicle/count?cityId
    */
  @ApiOperation("Conta veículos de um município")
  @GetMapping(path = Array("/count"))
  def count(@RequestParam cityId: String) =
  service.count(cityId)

  /**
    * GET /api/vehicle?id
    */
  @ApiOperation("Recupera um veículo")
  @GetMapping
  def getOrCreate(@RequestParam id: Int) =
  service.get(id)

  /**
    * PUT /api/vehicle
    */
  @ApiOperation("Salva ou atualiza veículo")
  @PutMapping
  @PreAuthorize("hasRole('ROLE_WRITE')")
  def saveOrUpdate(implicit principal: Authentication, @RequestBody command: Vehicle) =
  service.saveOrUpdate(_identityId, command)

}
