package pr.tce.comb.controller

import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation._
import pr.tce.comb.UserPrefs
import pr.tce.comb.service.UserPrefsService

/**
  * Controlador de preferências de usuários.
  *
  * <p>Facilita a navegação ao "lembrar" as últimas escolhas de um usuário.</p>
  *
  * @param service
  *
  * Created by mauriciofernandesdecastro on 27/05/17.
  */
@RestController
@RequestMapping(Array("/api/user/prefs"))
class UserPrefsController(service: UserPrefsService) {

  /**
    * GET /api/user/prefs?id
    */
  @ApiOperation("Recupera uma preferência.")
  @GetMapping
  def getOrCreate(@RequestParam id: String) = service.get(id)

  /**
    * PUT /api/user/prefs
    */
  @ApiOperation("Salva ou atualiza uma preferência.")
  @PutMapping
  def saveOrUpdate(@RequestBody command: UserPrefs) = service.saveOrUpdate(command)

}
