package pr.tce.comb.controller

import io.swagger.annotations.ApiOperation
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation._
import pr.tce.comb.CityData
import pr.tce.comb.service.CityDataService
import pr.tce.comb.util.AuthorityExtractor

/**
  * Dados complementares de municípios são necessários para tratar das
  * especificidades do sistema de controle de uso de veículos.
  *
  * <p>Por exemplo, na comparação entre o conusmo de duas cidades, uma pode ter
  * geografia mais acidentada, ou pode ter um número maior de habitantes em zonas
  * rurais.</p>
  *
  * <p>Caso não haja ainda um dado complementar para uma certa cidade, obtida
  * através do módulo IAM, ele é automaticamente criado.</p>
  *
  * <p>Este serviço é protegido. Somente usuários com autorização ROLE_WRITE
  * podem acessá-lo.</p>
  *
  * @param service colabora com a camada de serviços.
  *
  * Created by mauriciofernandesdecastro on 27/05/17.
  */
@RestController
@RequestMapping(Array("/api/city/data"))
@PreAuthorize("hasRole('ROLE_WRITE')")
class CityDataController(service: CityDataService) extends AuthorityExtractor {

  /**
    * POST /api/city/data/search
    */
  @ApiOperation("Pesquisa municípios e dados complementares")
  @PostMapping(path = Array("/search"))
  def search(@RequestBody sample: CityData) =
  service.search(sample)

  /**
    * GET /api/city/data/exclusion
    */
  @ApiOperation("Lista municípios excluídos")
  @GetMapping(path = Array("/exclusion"))
  def search(@RequestParam year: String) =
  service.exclusion(year)

  /**
    * GET /api/city/data?cityId
    */
  @ApiOperation("Recupera (ou cria) dados complementares")
  @GetMapping
  def getOrCreate(@RequestParam cityId: String) =
  service.get(cityId)

  /**
    * PUT /api/city/data
    */
  // TODO completar camada de serviços.
  @ApiOperation("Salva ou atualiza dados complementares")
  @PutMapping
  def saveOrUpdate(implicit principal: Authentication, @RequestBody command: CityData) =
    service.saveOrUpdate(_identityId, command)

}
