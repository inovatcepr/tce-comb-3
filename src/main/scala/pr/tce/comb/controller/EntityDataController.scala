package pr.tce.comb.controller

import io.swagger.annotations.ApiOperation
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation._
import pr.tce.comb.EntityData
import pr.tce.comb.chart.PieChartAdapter
import pr.tce.comb.service.EntityDataService
import pr.tce.comb.util.AuthorityExtractor

/**
  * Dados complementares de entidades são necessários para tratar das
  * especificidades como número de funcionários, etc.
  *
  * @param service colabora com a camada de serviços.
  *
  * Created by mauriciofernandesdecastro on 27/05/17.
  */
@RestController
@RequestMapping(Array("/api/entity/data"))
@PreAuthorize("hasRole('ROLE_WRITE')")
class EntityDataController(service: EntityDataService) extends AuthorityExtractor {

  /**
    * GET /api/entity/data/list?cityId
    */
  @ApiOperation("Lista entidades e dados complementares por cidade")
  @GetMapping(path = Array("/list"), params = Array("cityId"))
  def list(@RequestParam cityId: String) =
    service.list(cityId)

  /**
    * GET /api/entity/data?cityId
    */
  @ApiOperation("Recupera (ou cria) dados complementares")
  @GetMapping
  def getOrCreate(@RequestParam entityId: String) =
    service.get(entityId)

  /**
    * PUT /api/city/data
    */
  @ApiOperation("Salva ou atualiza dados complementares")
  @PutMapping
  def saveOrUpdate(implicit principal: Authentication, @RequestBody command: EntityData) =
    service.saveOrUpdate(_identityId, command)

}
