package pr.tce.comb.controller

import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.{GetMapping, RequestMapping, RequestParam, RestController}
import pr.tce.comb.service.PriceService

/**
  * Precifica o uso do combustível.
  *
  * Created by mauriciofernandesdecastro on 24/06/17.
  */
@RestController
@RequestMapping(Array("/api/price"))
class PriceController(service: PriceService) {

  /**
    * GET /api/price/entity
    */
  @ApiOperation("Agrega totais, mínimo e máximo de consumo de uma entidade")
  @GetMapping(path = Array("/entity"))
  def entity(@RequestParam entityId: String, @RequestParam year: Int) = service.aggregate(entityId, year)

}

