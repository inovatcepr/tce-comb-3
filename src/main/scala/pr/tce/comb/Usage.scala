package pr.tce.comb

import org.springframework.data.annotation.Id

/**
  * Dados próprios de um veículo.
  *
  * Created by mauriciofernandesdecastro on 21/05/17.
  */
class Usage(

  @Id
  var _id: String,

  var year: String,

  var cdIBGE: String = "",

  var nmMunicipio: String = "",

  var idEntidade: String = "",

  var nmEntidade: String = "",

  var nrAno: String,

  var nrMes: String,

  var idVeiculoEquipamento: String,

  var dsTiposObjetoDespesa: String,

  var vlConsumo: Float,

  var idTipoMedidor: String,

  var ultimoEnvioSIMAMNesteExercicio: String

)
