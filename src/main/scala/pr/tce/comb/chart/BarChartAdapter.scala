package pr.tce.comb.chart

/**
  * Adaptador para o gráfico de barras.
  */
case class BarChartAdapter(usageMap: Map[String, Double], options: Option[ChartOptions] = None) extends GoogleChartWrapper {

  val names = usageMap.keys.toList

  val v = usageMap.values.toList

  println(v.mkString("|"))

  val cols = List(ChartColumn("t", "Resultado"), ChartColumn("q", "Despesa", "number"))

  override def getType: String = "BarChart"

  def getOptions = options

}