package pr.tce.comb.chart

/**
  * Adaptador para o gráfico de pizza.
  */
case class PieChartAdapter(usageMap: Map[String, Double] = Map()) extends GoogleChartWrapper {

  val names = usageMap.keys.toList

  val v = usageMap.values.toList

  val colors = List("#FF9900", "#109618", "#DC3912", "#990099", "#AAAA11")

  val cols = List(ChartColumn("t", "Resultado"), ChartColumn("q", "Qtde", "number"))

  override def getType: String = "PieChart"

  def getOptions = ChartOptions(title="Uso de Veículos", colors=colors, legend="labeled", pieHole=0.6)

}