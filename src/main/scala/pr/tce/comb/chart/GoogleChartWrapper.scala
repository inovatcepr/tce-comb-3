package pr.tce.comb.chart

import scala.beans.BeanProperty

/**
  * Mix-in to a basic google chart.
  */
trait GoogleChartWrapper {

  val names: List[String]

  val cols: List[ChartColumn]

  val v: List[Double]

  def getData: ChartData = ChartData(cols,values(v))

  def getType: String

  def values1(qtys: List[Int]): List[_ <: Any] = {
    val q:List[ChartValueNumber] = List(ChartValueNumber(1),ChartValueNumber(2),ChartValueNumber(3),ChartValueNumber(4),ChartValueNumber(5))//qtys.map(x => ChartValueNumber(x.asInstanceOf[Int]))
    val p = names.map(ChartValueName(_)).zip(q).flatMap(e => List(e._1, e._2))
    p
  }

  def values(qtys: List[Double]): List[ChartRow] = {
    names.zipWithIndex.map(s => ChartRow(List(ChartValueName(s._1), ChartValueNumber(v(s._2)))))
  }

}
case class ChartColumn(@BeanProperty id:String, @BeanProperty label:String, @BeanProperty `type`:String = "string")
case class ChartData(@BeanProperty cols:List[ChartColumn], @BeanProperty rows:List[ChartRow])
case class ChartRow(@BeanProperty c:List[_ <: Any])

trait ChartValue { val v: Any }
case class ChartValueName(@BeanProperty v: String) extends ChartValue
case class ChartValueNumber(@BeanProperty v: Double) extends ChartValue









