package pr.tce.comb.chart

import scala.beans.BeanProperty

case class ChartOptions
(
  title: String = "",
  height: String = "200",
  colors: List[String] = List(),
  legend: String = "right",
  pieHole: Double = 0.0,
  fontSize: Int = 11,
//  chartArea: ChartArea = ChartArea(),
  is3D: Boolean = false,
  vAxis: Option[VAxis] = None,
  animation: Option[Animation] = None
) {
  def getvAxis = vAxis match {
    case Some(v) => v
    case None => null
  }
  def getAnimation = animation match {
    case Some(a) => a
    case None => Animation(0)
  }
  val getSliceVisibilityThreshold = 0
  val getPieSliceTextStyle = ChartTextStyle()
}
case class Animation(duration:Int)
case class ChartArea(left:Int=0, top:Int=20, width:String="90%", height:String="90%")
case class ChartTextStyle(color:String="black", fontSize: Int = 36)
case class VAxis(maxValue: Double, title: String = "", textPosition: String = "out")