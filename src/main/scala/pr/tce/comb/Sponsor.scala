package pr.tce.comb

import org.springframework.data.annotation.Id

/**
  * Perfil do patrocinador.
  *
  * Created by mauriciofernandesdecastro on 24/06/17.
  */
class Sponsor (

  @Id
  var _id: String,

  var fullName: String,

  var role: String,

  var testimonial: String

)
