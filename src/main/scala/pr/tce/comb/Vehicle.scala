package pr.tce.comb

import org.springframework.data.annotation.Id

/**
  * Dados de um veículo.
  *
  * Created by mauriciofernandesdecastro on 21/05/17.
  */
class Vehicle(

  @Id
  var _id: String = "",

  var cdIBGE: String = "",

  var nmMunicipio: String = "",

  var idEntidade: String = "",

  var nmEntidade: String = "",

  var cdBem: String = "",

  var dsTipoPropriedadeBem: String = "",

  var nrPlaca: String = "",

  var dsMarcaVeiculo: String = "",

  var dsModeloFIPE: String = "",

  var dsVeiculoEquipamento: String = ""

)