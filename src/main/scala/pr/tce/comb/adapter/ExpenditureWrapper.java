package pr.tce.comb.adapter;

public class ExpenditureWrapper {

    private Double total = 0.0;

    private String key;

    private String name;

    public ExpenditureWrapper() {
    }

    public ExpenditureWrapper(Object rawTotal, String key, String name) {
        this();
        if (rawTotal instanceof Double) {
            setTotal((Double)rawTotal);
        }
        setKey(key);
        setName(name);
    }

    public Double getTotal() {
        return this.total;
    }

    public String getKey() {
        return this.key;
    }

    public String getName() {
        return this.name;
    }

    public void setTotal(Double rawTotal) {
        this.total = rawTotal;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isValid() {
        return total < 1.0E9;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof ExpenditureWrapper)) return false;
        final ExpenditureWrapper other = (ExpenditureWrapper) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$rawTotal = this.getTotal();
        final Object other$rawTotal = other.getTotal();
        if (this$rawTotal == null ? other$rawTotal != null : !this$rawTotal.equals(other$rawTotal)) return false;
        final Object this$key = this.getKey();
        final Object other$key = other.getKey();
        if (this$key == null ? other$key != null : !this$key.equals(other$key)) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        return true;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $rawTotal = this.getTotal();
        result = result * PRIME + ($rawTotal == null ? 43 : $rawTotal.hashCode());
        final Object $key = this.getKey();
        result = result * PRIME + ($key == null ? 43 : $key.hashCode());
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof ExpenditureWrapper;
    }

    public String toString() {
        return "pr.tce.comb.adapter.ExpenditureWrapper(rawTotal=" + this.getTotal() + ", key=" + this.getKey() + ", name=" + this.getName() + ")";
    }
}
