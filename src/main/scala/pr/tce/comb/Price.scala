package pr.tce.comb

import org.springframework.data.annotation.Id

/**
  * Created by mauriciofernandesdecastro on 24/06/17.
  */
class Price(

  @Id
  var _id: String,

  var idEntidade: String,

  var cdIBGE: String,

  var nmMunicipio: String,

  var nrAnoLiquidacao: Int,

  var nrMesLiquidacao: Int,

  var dtLiquidacao: String,

  var dsTipoObjetoDespesa: String,

  var vlLiquidacao: Double,

  var nrQuantidadeConsolidadaLiquidacao: Double,

  var vlUnitarioLiquidacao: Double

)
