package pr.tce.comb

import org.springframework.data.annotation.Id

/**
  * Dados complementares de entidades.
  *
  * Created by mauriciofernandesdecastro on 26/05/17.
  */
class EntityData(

  @Id
  var _id: String = "",

  var cdIBGE: String = "",

  var nmEntidade: String = ""

)
