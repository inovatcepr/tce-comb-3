package pr.tce.comb.service

import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query.query
import org.springframework.stereotype.Service
import pr.tce.comb.EntityData


/**
  * Serviço de dados complementares de entidade.
  */
@Service
class EntityDataService(template: MongoTemplate) {

  /**
    * Pesquisa entidades por cidade.
    */
  def list(cityId: String) = template.find(query(where("cdIBGE").is(cityId)), classOf[EntityData])

  /**
    * Recupera uma entidade.
    */
  def get(entityId: String): EntityData = template.findById(entityId, classOf[EntityData], "entityData")

  /**
    * Salva ou atualiza uma entidade.
    */
  def saveOrUpdate(identityId: String, command: EntityData) = template.save(command, "entityData")

}
