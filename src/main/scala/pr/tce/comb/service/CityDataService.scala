package pr.tce.comb.service

import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query.query
import org.springframework.stereotype.Service
import pr.tce.comb.{CityData, CityExclusion}


/**
  * Serviço de dados complementares de município.
  */
@Service
class CityDataService(template: MongoTemplate) {

  //
  // TODO será preciso considerar as cidades excluídas devido à inconsistência nos dados
  //
  // Vide classe de domínio CityExclusion - considerar incprporar à classe CityData
  // Para efeitos dessa versão consideraremos a tabela conhecida abaixo:
  //

  var cityExclusions: List[CityExclusion] = List(
    new CityExclusion("410480", "410480", "Cascavel", "2016", "Total de gastos muito acima do possível, provável erro."),
    new CityExclusion("411950", "411950", "Piraquara", "2016", "Total de gastos muito acima do possível, provável erro."),
    new CityExclusion("410754", "410754", "Espigão Alto do Iguaçu", "2016", "Área não informada.")
  )

  /**
    * Pesquisa de cidades.
    */
  def search(sample: CityData) = template.find(query(where("nmMunicipio").regex(s".*${sample.nmMunicipio}.*")), classOf[CityData])

  /**
    * CIdades excluídas.
    */
  def exclusion(year: String = "2016") = cityExclusions

  /**
    * Recupera uma cidade por id.
    */
  def get(cityId: String): CityData = template.findById(cityId, classOf[CityData], "cityData")

  // TODO salvar ou atualizar dados de uma cidade
  def saveOrUpdate(identityId: String, command: CityData): CityData = ???

}
