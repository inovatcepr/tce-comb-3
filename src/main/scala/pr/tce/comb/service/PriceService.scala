package pr.tce.comb.service

import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.data.mongodb.core.aggregation.Aggregation.{group, newAggregation, project, sort, _}
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.stereotype.Service
import pr.tce.comb.{Accumulator, Calculator, PriceWrapper}

/**
  * Serviço quantificação do uso do combustível.
  */
@Service
class PriceService(template: MongoTemplate) {

  import collection.JavaConverters._

  /**
    * Calcula estatísticas de preço no ano, conforme é pago em cada entidade.
    *
    * @param entityId
    * @param year
    */
  // TODO avaliar se pode usar despesas
  def aggregate(entityId: String, year: Int) = {
    val agg: Aggregation = newAggregation(
      `match`(Criteria.where("idEntidade").is(entityId).and("nrAnoLiquidacao").is(year))
      , group("dsTipoObjetoDespesa")
        .avg("vlUnitarioLiquidacao").as("avg")
        .min("vlUnitarioLiquidacao").as("min")
        .max("vlUnitarioLiquidacao").as("max")
      , project("avg", "min", "max").and("_id").as("key")
      , sort(Sort.DEFAULT_DIRECTION, "key"))
    template.aggregate(agg, "price", classOf[Calculator]).getMappedResults.asScala
  }

}
