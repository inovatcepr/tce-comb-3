package pr.tce.comb.service

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Service
import pr.tce.comb.{Developer, Sponsor}

import scala.io.Source

/**
  * Serviço de dados da equipe.
  */
@Service
class TeamService() {

  /**
    * Load team from json file.
    */
  def loadTeam(teamType: String) = teamType match {
    case "dev" =>
      val teamData = Source.fromInputStream(getClass().getResourceAsStream("/developers.json")).mkString
      new ObjectMapper().readValue(teamData, classOf[java.util.List[Developer]])
    case "sponsor" =>
      val teamData = Source.fromInputStream(getClass().getResourceAsStream("/sponsors.json")).mkString
      new ObjectMapper().readValue(teamData, classOf[java.util.List[Sponsor]])
  }

}
