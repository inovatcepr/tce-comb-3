package pr.tce.comb.service

import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.{Criteria, Query}
import org.springframework.stereotype.Service
import pr.tce.comb.UserPrefs

@Service
class UserPrefsService(template: MongoTemplate) {

  /**
    * Lista preferências de usuário de uma entidade.
    */
  def list(entityId: String) = {
    val byEntityId = new Query().addCriteria(Criteria.where("entityId").is(entityId))
    template.find(byEntityId, classOf[UserPrefs])
  }

  /**
    * Recuperar uma preferência.
    */
  def get(userId: String): UserPrefs = template.findById(userId, classOf[UserPrefs])

  /**
    * Salvar ou atualizar uma preferência.
    */
  def saveOrUpdate(command: UserPrefs) = template.save(command, "userPrefs")

}
