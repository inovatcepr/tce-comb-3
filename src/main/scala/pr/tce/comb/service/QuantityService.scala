package pr.tce.comb.service

import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.data.mongodb.core.aggregation.Aggregation.{group, newAggregation, project, sort, _}
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.stereotype.Service
import pr.tce.comb._

/**
  * Serviço de uso de veículos.
  *
  * Created by mauriciofernandesdecastro on 27/05/17.
  */
@Service
class QuantityService(template: MongoTemplate) {

  import collection.JavaConverters._

  /**
    * Agrega o consumo de combustível de veículos por entidade e ano.
    */
  def aggregate(entityId: String, year: Int) = {
    val agg: Aggregation = newAggregation(
      `match`(Criteria.where("idEntidade").is(entityId).and("nrAnoConsumo").is(year))
      , group("dsTipoObjetoDespesaNrSeq")
        .sum("nrQuantidadeConsumoVeiculoMensal").as("total")
      , project("total").and("_id").as("key")
      , sort(Sort.DEFAULT_DIRECTION, "key"))
    template.aggregate(agg, "expenditure", classOf[Accumulator]).getMappedResults.asScala
  }

  /**
    * Agrega a despesa de combustível de veículos por cidade e ano.
    */
  def byCity(cityId: String, year: Int) = {
    val p = newAggregation(`match`(Criteria.where("cdIBGE").is(cityId).and("nrAnoLiquidacao").is(year))
      , group("cdIBGE", "nrAnoLiquidacao").sum("vlMensalDespesa").as("total").first("populacao").as("populacao").first("area").as("area"))
    template.aggregate(p, "expenditure", classOf[CityExpenditureWrapper]).getUniqueMappedResult
  }

  // TODO
  def list(vehicleId: String): java.util.List[Usage] = ???

  // TODO
  def get(id: String): Usage = ???

  // TODO
  def saveOrUpdate(_identityId: String, command: Usage): Usage = ???

}
