package pr.tce.comb.service

import org.slf4j.LoggerFactory
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.data.mongodb.core.aggregation.Aggregation.{group, newAggregation, project, sort, _}
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query.query
import org.springframework.stereotype.Service
import pr.tce.comb.adapter.ExpenditureWrapper
import pr.tce.comb.chart.{BarChartAdapter, ChartOptions, PieChartAdapter}
import pr.tce.comb.{CityExclusion, Expenditure}

import scala.collection.immutable.ListMap


/**
  * Serviço de despesas.
  */
@Service
class ExpenditureService(template: MongoTemplate) {

  val logger = LoggerFactory.getLogger(classOf[ExpenditureService])

  val errorThreshold = 1000000000

  //
  // TODO será preciso considerar as cidades excluídas devido à inconsistência nos dados
  //
  // Vide classe de domínio CityExclusion - considerar incprporar à classe CityData
  // Para efeitos dessa versão consideraremos a tabela conhecida abaixo:
  //

  var cityExclusions: List[CityExclusion] = List(
    new CityExclusion("410480", "410480", "Cascavel", "2016", "Total de gastos muito acima do possível, provável erro."),
    new CityExclusion("411950", "411950", "Piraquara", "2016", "Total de gastos muito acima do possível, provável erro."),
    new CityExclusion("410754", "410754", "Espigão Alto do Iguaçu", "2016", "Área não informada.")
  )

  def byCity(cityId: String, year: String) =
    template.find(query(where("nrAnoConsumo").is(year).and("cdIBGE").is(cityId)), classOf[Expenditure])

  import collection.JavaConverters._

  /**
    * Apresenta as agregações totais de gastos de combustível em todas as cidades, respeitando exclusões.
    */
  def total(slice: Int, skip: Int, ordered: String = "DESC", aggregate: String) = {
    val agg: Aggregation = newAggregation(
      `match`(Criteria.where("cdIBGE").not().in(cityExclusions.map(_._id):_*).and("vlMensalDespesa").lt(errorThreshold))
      , group("cdIBGE", "nmMunicipio").sum(aggregate).as("total")
      , project("total").and("_id.cdIBGE").as("key").and("_id.nmMunicipio").as("name")
      , sort(Sort.Direction.fromString(ordered), "total"))
    template.aggregate(agg, "expenditure", classOf[ExpenditureWrapper]).getMappedResults.asScala
      .filter(_.isValid).slice(skip, slice + skip).toList
  }

  /**
    * Apresenta as agregações totais de gastos de combustível por cidade.
    */
  def byCity(cityId: String, ordered: String = "DESC", aggregate: String) = {
    val agg: Aggregation = newAggregation(
      `match`(Criteria.where("cdIBGE").is(cityId))
      , group("idEntidade").sum(aggregate).as("total")
      , project("total").and("_id").as("key").and("_id.nmMunicipio").as("name")
      , sort(Sort.Direction.fromString(ordered), "total"))
    template.aggregate(agg, "expenditure", classOf[ExpenditureWrapper]).getMappedResults.asScala
      .toList
  }

  /**
    * Apresenta as agregações totais de gastos de combustível por entidade.
    */
  def byEntity(entityId: String) = {
    logger.info(s"Agregação para $entityId")
    val agg: Aggregation = newAggregation(
      `match`(Criteria.where("idEntidade").is(entityId))
      , group("dsTipoObjetoDespesa").sum("vlMensalDespesa").as("total")
      , project("total").and("_id").as("key").and("_id").as("name")
      , sort(Sort.Direction.fromString("DESC"), "total"))
    template.aggregate(agg, "expenditure", classOf[ExpenditureWrapper]).getMappedResults.asScala
      .toList
  }

  /**
    * Apresenta as agregações médias de gastos de combustível.
    */
  def avg(slice: Int, skip: Int, ordered: String = "DESC", aggregate: String) = {
    val agg: Aggregation = newAggregation(
      `match`(Criteria.where("cdIBGE").not().in(cityExclusions.map(_._id):_*).and("vlMensalDespesa").lt(errorThreshold))
      , group("cdIBGE", "nmMunicipio").avg(aggregate).as("total")
      , project("total").and("_id.cdIBGE").as("key").and("_id.nmMunicipio").as("name")
      , sort(Sort.Direction.fromString(ordered), "total"))
    template.aggregate(agg, "expenditure", classOf[ExpenditureWrapper]).getMappedResults.asScala
      .filter(_.isValid).slice(skip, slice + skip).toList
  }

  /**
    * Formata um gráfico de pizza.
    */
  def chartPie(entityId: String) = Option(entityId) match {
      case Some(id) =>
        val graph = byEntity(entityId)
        val data = graphToMap(graph)
        PieChartAdapter(data)
      case None => PieChartAdapter
    }

  /**
    * Formata um gráfico de barras de acordo com o tipo.
    */
  def chartBar(chartRequest: ChartRequest) = {
    chartRequest.chart match {
      case 0 =>
        val graph = total(chartRequest.slice, chartRequest.skip, chartRequest.ordered, "vlMensalDespesa")
        chartAs(graph, chartRequest.color, 1000, 30*chartRequest.slice)
      case 1 =>
        val graph = avg(chartRequest.slice, chartRequest.skip, chartRequest.ordered, "porHabitante")
        chartAs(graph, chartRequest.color, 1, 30*chartRequest.slice)
      case 2 =>
        val graph = avg(chartRequest.slice, chartRequest.skip, chartRequest.ordered, "porArea")
        chartAs(graph, chartRequest.color, 1, 30*chartRequest.slice)
    }
  }

  /**
    * Gráfico de barras.
    */
  private def chartAs(graph: List[ExpenditureWrapper], color: String, divider: Double = 1, height: Int = 200) = {
    val data = graphToMap(graph)
    val options = ChartOptions(title = "Despesa", colors = List(color), legend = "none", height = height.toString)
    BarChartAdapter(data, Some(options))
  }

  /**
    * Converte uma lista de despesas em um mapa para o processamento dos gráficos.
    */
  private def graphToMap(graph: List[ExpenditureWrapper]) =
    graph.foldLeft(ListMap[String, Double]()) { (m, e) => m ++ ListMap(e.getName -> e.getTotal/1000) }

}
case class ChartRequest(chart: Int, slice: Int, skip: Int, ordered: String, color: String)
