package pr.tce.comb.service

import org.springframework.data.domain.Sort.Direction.DESC
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.data.mongodb.core.aggregation.Aggregation._
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query.query
import org.springframework.stereotype.Service
import pr.tce.comb.{Counter, CounterWrapper, Vehicle}

/**
  * Serviço para veículos.
  *
  * Created by mauriciofernandesdecastro on 27/05/17.
  */
@Service
class VehicleService(template: MongoTemplate) {

  def list(entityId: String) = template.find(query(where("idEntidade").is(entityId)), classOf[Vehicle])

  def count(cityId: String) = {
    val agg: Aggregation = newAggregation(
      `match`(Criteria.where("cdIBGE").is(cityId))
      , group("idEntidade").count.as("total")
      , project("total").and("_id").as("key")
      , sort(DESC, "total"))
    import collection.JavaConverters._
    new CounterWrapper(template.aggregate(agg, "vehicle", classOf[Counter]).getMappedResults.asScala)
  }

  // TODO
  def get(entityId: Any) = ???

  // TODO
  def saveOrUpdate(_identityId: String, command: Vehicle) = ???

}
