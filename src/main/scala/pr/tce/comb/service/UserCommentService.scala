package pr.tce.comb.service

import org.springframework.stereotype.Service
import pr.tce.comb.UserComment

/**
  * Created by mauriciofernandesdecastro on 27/05/17.
  */
@Service
class UserCommentService {

  // TODO
  def list(commentPath: String): java.util.List[UserComment] = ???

  // TODO
  def get(id: String): UserComment = ???

  // TODO
  def saveOrUpdate(_identityId: String, command: UserComment) = ???

}
