package pr.tce.comb

import java.util.Date

import org.springframework.data.annotation.Id

/**
  * Comentários de usuário.
  *
  * Created by mauriciofernandesdecastro on 27/05/17.
  */
class UserComment (

  @Id
  var _id: String = "",

  var identityId: String,

  var commentPath: String,

  var commentBody: String = "",

  var issueDate: Date = new Date()

)
