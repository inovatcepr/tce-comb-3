package pr.tce.comb

import scala.util.Try

/**
  * Conveniente para realizar cálculos.
  */
case class Counter(var total: Int, var key: String) {
  def toMap(grandTotal: Long) = Map (
    "total" -> total.toString
    , "percent" -> f"${total.toFloat * 100 / grandTotal}%2.2f"
    , "permille" -> s"${total.toFloat * 1000 / grandTotal}"
  )
}
class CounterWrapper(counter: Iterable[Counter] = List[Counter]()) {
  val grandTotal = counter.foldLeft(0)(_ + _.total)
  val counterMap = counter.foldLeft(Map[String, Map[String, String]]()) { (m, v) =>
    m + (v.key -> v.toMap(grandTotal))
  }
}

case class Accumulator(var total: Double, var key: String, var fuel: String = "") {
}

class Calculator(var avg: Double, var min: Double, var max: Double, var key: String) {
}
case class PriceWrapper(idEntidade: String, dsTipoObjetoDespesa: String, price: Double = 0)
case class QuantityWrapper(idEntidade: String, dsTiposObjetoDespesa: String, total: Int, price: java.lang.Double = null) {

  def matches(other: PriceWrapper) =
    idEntidade == other.idEntidade && (dsTiposObjetoDespesa.contains(other.dsTipoObjetoDespesa) || other.dsTipoObjetoDespesa.contains(dsTiposObjetoDespesa))
  val expense: Double = Option(price) match {
    case Some(p) => p * total
    case None => 0
  }
}
case class SummaryWrapper(usages: Iterable[QuantityWrapper]) {
  val totalExpense = usages.foldLeft(0.0)(_ + _.expense)
}
//case class ExpenditureWrapper(var rawTotal: java.lang.Object, var key: String, var name: String) {
//  val total = Try(rawTotal.asInstanceOf[Double]).getOrElse(0.toDouble)
//  val valid = total < 1.0E9
//}

case class CityExpenditureWrapper(entityId: String, year: Integer, total: java.lang.Double, area: Int, populacao: java.lang.Double)