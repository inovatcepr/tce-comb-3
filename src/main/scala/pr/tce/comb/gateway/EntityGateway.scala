package pr.tce.comb.gateway

import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{GetMapping, RequestMapping, RequestParam}

/**
  * Lista indivíduos existentes via API do IAM.
  *
  * <p>Os indivíduos são criados quando ingressam no cadastro.</p>
  *
  * <p>A definição dos parâmetros de serviço do IAM é injetada de acordo com
  * o conteúdo do arquivo application.yaml.</p>
  *
  * Created by mauriciofernandesdecastro on 27/05/17.
  */
// TODO implementar IAM
@Controller
@RequestMapping(Array("/api/identity"))
class EntityGateway {

  @Value("${helianto.iam.url}")
  val iamService: String = ""

  val identityEndpoint = "/api/identity"

  /**
    * GET /api/identity?id -> GET /api/identity?id
    */
  @ApiOperation("Encaminha requisição para recuperar indivíduo.")
  @GetMapping
  def get(@RequestParam id: String) = s"""forward:$iamService$identityEndpoint?id=$id"""

}
