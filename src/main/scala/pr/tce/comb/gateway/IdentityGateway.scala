package pr.tce.comb.gateway

import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{GetMapping, RequestMapping}

/**
  * Lista entidades existentes via API do IAM.
  *
  * <p>As entidades são criadas a partir da leitura dos arquivos de dados do cliente.</p>
  *
  * <p>A definição dos parâmetros de serviço do IAM é injetada de acordo com
  * o conteúdo do arquivo application.yaml.</p>
  *
  * Created by mauriciofernandesdecastro on 27/05/17.
  */
// TODO implementar IAM
@Controller
@RequestMapping(Array("/api/entity"))
class IdentityGateway {

  @Value("${helianto.iam.url}")
  val iamService: String = ""

  val entityEndpoint = "/api/entity"

  /**
    * GET /api/entity/list -> GET /api/entity/list
    */
  @ApiOperation("Encaminha requisição para listar entidades.")
  @GetMapping(path = Array("/list"))
  def list() = s"""forward:$iamService$entityEndpoint/list"""

}
