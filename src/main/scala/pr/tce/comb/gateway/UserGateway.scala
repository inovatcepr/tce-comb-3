package pr.tce.comb.gateway

import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{GetMapping, RequestMapping, RequestParam}

/**
  * Gerencia usuários existentes via API do IAM.
  *
  * <p>Os usuários são criados a partir do registro no IAM.</p>
  *
  * <p>A definição dos parâmetros de serviço do IAM é injetada de acordo com
  * o conteúdo do arquivo application.yaml.</p>
  *
  * Created by mauriciofernandesdecastro on 27/05/17.
  */
// TODO implementar IAM
@Controller
@RequestMapping(Array("/api/user"))
class UserGateway {

  @Value("${helianto.iam.url}")
  val iamService: String = ""

  val userEndpoint = "/api/user"

  /**
    * GET /api/user/list -> GET /api/user/list
    */
  @ApiOperation("Encaminha requisição para listar usuários.")
  @GetMapping(path = Array("/list"))
  def list() = s"""forward:$iamService$userEndpoint/list"""

  /**
    * GET /api/user?id -> GET /api/user?id
    */
  @ApiOperation("Recupera um usuário")
  @GetMapping
  def getOrCreate(@RequestParam id: Int) = s"""forward:$iamService$userEndpoint?id=$id"""

}
