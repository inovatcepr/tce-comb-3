package pr.tce.comb.gateway

import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{GetMapping, RequestMapping}

/**
  * Lista municípios existentes via API do IAM.
  *
  * <p>Os municípios - assim com UFs - são criados automaticamente quando da
  * instalação do IAM.</p>
  *
  * <p>A definição dos parâmetros de serviço do IAM é injetada de acordo com
  * o conteúdo do arquivo application.yaml.</p>
  *
  * Created by mauriciofernandesdecastro on 27/05/17.
  */
// TODO implementar IAM
@Controller
@RequestMapping(Array("/api/city"))
class CityGateway {

  @Value("${helianto.iam.url}")
  val iamService: String = ""

  @Value("${application.defaults.UF:'PR'}")
  val defaultUF: String = ""

  val cityEndpoint = "/api/context/city"

  /**
    * GET /api/city/list -> GET {iamService}/{cityEndpoint}/{defaultUF}
    */
  @ApiOperation("Encaminha requisição para listar cidades.")
  @GetMapping(path = Array("/list"))
  def list = s"""forward:$iamService$cityEndpoint/$defaultUF"""

}
