package pr.tce.comb

import org.springframework.data.annotation.Id

/**
  * Perfil do desenvolvedor.
  *
  * Created by mauriciofernandesdecastro on 24/06/17.
  */
class Developer (

  @Id
  var _id: String,

  var principal: String,

  var makePrincipalPublic: Boolean,

  var fullName: String,

  var imageUrl: String,

  var companyName: String,

  var companyLogoUrl: String,

  var companySiteUrl: String,

  var facebookProfile: String,

  var linkedInProfile: String,

  var twitterProfile: String,

  var gitHubProfile: String,

  var testimonial: String

)
