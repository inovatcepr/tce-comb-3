package pr.tce.comb

import org.springframework.data.annotation.Id

/**
  * Created by mauriciofernandesdecastro on 05/07/17.
  */
class CityExclusion (

  @Id
  var _id: String = "",

  var cdIBGE: String = "",

  var nmMunicipio: String = "",

  var year: String = "",

  var motive: String = ""

)
