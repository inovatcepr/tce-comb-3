package pr.tce.comb.util

import org.springframework.context.annotation.{Bean, Configuration}
import springfox.documentation.builders.{PathSelectors, RequestHandlerSelectors}
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

/**
  * Configtura a documentação da API com Swagger.
  */
@EnableSwagger2
@Configuration
class SwaggerUtils {

  @Bean
  def rootApi: Docket = new Docket(DocumentationType.SWAGGER_2)
    .select()
    .apis(RequestHandlerSelectors.any())
    .paths(PathSelectors.ant("/api/**"))
    .build.pathMapping("/")

}
