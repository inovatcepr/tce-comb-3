package pr.tce.comb.util

import org.springframework.security.core.Authentication

/**
  * Mix-in to implicitly extract entity or identity from the principal.
  */
trait AuthorityExtractor {

  def _contextName(implicit principal: Authentication) = id(principal, "CONTEXT_")

  def _entityId(implicit principal: Authentication) = id(principal, "ENTITY_ID_")

  def _userId(implicit principal: Authentication) = id(principal, "USER_ID_")

  def _identityId(implicit principal: Authentication) = id(principal, "SELF_ID_")

  def _authoritySet(implicit principal: Authentication) = {
    import collection.JavaConversions._
    principal
      .getAuthorities
      .map(_.toString)
  }

  private def id(principal: Authentication, prefix: String): String =
    Option(principal) match {
      case Some(p) =>
        import collection.JavaConversions._
        p.getAuthorities
          .filter(_.toString.startsWith(prefix))
          .map(_.toString.substring(prefix.length))
          .headOption.getOrElse("")
      case None => ""
    }

}
