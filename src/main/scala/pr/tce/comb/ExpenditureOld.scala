package pr.tce.comb

import org.springframework.data.annotation.Id

/**
  * Gastos por cidade
  *
  * Created by mauriciofernandesdecastro on 21/05/17.
  */
class ExpenditureOld(

   @Id
   var _id: String = "",

   var year: String,

   var cityId: String,

   var entityId: String,

   var fuel: String,

   var usage: Double,

   var averagePrice: Double,

   var expenditure: Double,

   var expenditureByArea: Double,

   var expenditureByInhabitant: Double

)