package pr.tce.comb

import org.springframework.data.annotation.Id

/**
  * Preferências do usuário.
  *
  * Created by mauriciofernandesdecastro on 21/05/17.
  */
class UserPrefs (

  @Id
  val _id: String,

  val name: String

)
