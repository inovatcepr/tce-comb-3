package pr.tce.comb

import org.springframework.data.annotation.Id

/**
  * Dados complementares a municipios.
  *
  * Created by mauriciofernandesdecastro on 26/05/17.
  */
class CityData(

  @Id
  var _id: String = "",

  var cdIBGE: String = "",

  var nmMunicipio: String = ""

//  var naturezaTerreno: String = "PLANO", //"ACIDENTADO"
//
//  var area: Double = 0.0,
//
//  var populacao: Integer = 0

)
