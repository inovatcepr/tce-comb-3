package pr.tce.comb

import org.springframework.data.annotation.Id

/**
  * Gastos por cidade
  *
  * Created by mauriciofernandesdecastro on 21/05/17.
  */
class Expenditure(

   @Id
   var _id: String = "",

   var nrAnoConsumo: Int, // era year

   var nrMesConsumo: Int, // novo

   var cdIBGE: String, // era cityId

   var idEntidade: String, // era entityId

   var dsTipoObjetoDespesaNrSeq: String, // era fuel

   var nrQuantidadeConsumoVeiculoMensal: Double, // era usage

   var vlMedioLiquidacao: Double, // era averagePrice

   var vlMensalDespesa: Double, // era expenditure

   var populacao: Int, // expenditureByArea

   var area: Double, // expenditureByArea

   var porArea: Double, // expenditureByArea

   var porHabitante: Double // expenditureByInhabitant

)