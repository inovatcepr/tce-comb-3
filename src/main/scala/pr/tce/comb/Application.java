package pr.tce.comb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import pr.tce.comb.util.JacksonUtils;
import pr.tce.comb.util.SwaggerUtils;

@SpringBootApplication(scanBasePackages={"pr.tce.*.service","pr.tce.*.controller"})
@Import({JacksonUtils.class, SwaggerUtils.class})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
