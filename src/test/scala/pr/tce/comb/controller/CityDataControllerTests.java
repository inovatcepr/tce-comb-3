package pr.tce.comb.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import pr.tce.comb.StandardWebDriverConfiguration;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by mauriciofernandesdecastro on 29/05/17.
 */
@RunWith(SpringRunner.class)
@Import(StandardWebDriverConfiguration.class)
public class CityDataControllerTests {

    @Autowired
    private WebDriver webDriver;

    @Test
    public void cityData() {
        // GIVEN estado inicial da aplicação

        // WHEN acesso
        webDriver.get("http://localhost:8080/api/city/data?cityId=1");

//        // THEN it appears on the list
//        WebElement ul = webDriver.findElement(By.cssSelector("ul"));
//        List<WebElement> liElements = ul.findElements(By.cssSelector("li"));
//        assertEquals(1, liElements.size());
//        assertTrue(liElements.get(0).getText().contains("test todo"));
    }

}
