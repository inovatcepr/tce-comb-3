organization := "pr.tce"

name := "tce-com-3"

scalaVersion := "2.11.8"

sbtVersion := "0.13.13"

version := "0.2"

val bootVersion = "1.5.3.RELEASE"

lazy val root = (project in file("."))
  .enablePlugins(JavaServerAppPackaging)
  .enablePlugins(DockerPlugin)
  .settings(
    mainClass in Compile := Some("pr.tce.comb.Application"),
    packageName in Docker := "iservport/tce-comb-3",
    dockerBaseImage := "anapsix/alpine-java:latest",
    dockerUpdateLatest := true,
    dockerExposedPorts := Seq(8080),
    dockerExposedVolumes := Seq("/opt/data"),
    dockerRepository := Some("docker.io")
  )

// API dependencies
libraryDependencies ++= Seq(
  "org.springframework.boot"     % "spring-boot-starter-web"          % bootVersion,
  "org.springframework.boot"     % "spring-boot-starter-security"     % bootVersion,
  "org.springframework.boot"     % "spring-boot-starter-freemarker"   % bootVersion,
  "org.springframework.boot"     % "spring-boot-starter-data-mongodb" % bootVersion,

  // jackson json binding com scala
  "com.fasterxml.jackson.module" % "jackson-module-scala_2.11"        % "2.8.8",

  "com.lightbend.akka"          %% "akka-stream-alpakka-xml"          % "0.9",

  "org.apache.spark"  %% "spark-core"            % "2.1.0",
  "org.apache.spark"  %% "spark-sql"             % "2.1.0",
  "org.mongodb.spark" %% "mongo-spark-connector" % "2.0.0",
  "com.typesafe"       % "config"                % "1.3.1",
  "org.projectlombok"  % "lombok"                % "1.16.8",

  // documentação da API com Swagger
  "io.springfox"                 % "springfox-swagger2"               % "2.6.0",
  "io.springfox"                 % "springfox-swagger-ui"             % "2.6.0",
  "io.swagger"                   % "swagger-core"                     % "1.5.10",

  // testes
  "org.scalatest"               %% "scalatest"                        % "3.0.0"     % "test",
  "org.mockito"                  % "mockito-all"                      % "1.10.19"   % "test",
  "junit"                        % "junit"                            % "4.11"      % "test",
  "org.springframework.boot"     % "spring-boot-starter-test"         % bootVersion % "test",
  "org.seleniumhq.selenium"      % "selenium-java"                    % "3.4.0"     % "test",
  "org.seleniumhq.selenium"      % "selenium-chrome-driver"           % "3.4.0"     % "test",
  "io.github.bonigarcia"         % "webdrivermanager"                 % "1.6.2"     % "test",

  // logging
  "org.slf4j"                    % "slf4j-simple"                   % "1.7.14"

)

// Front-end dependencies
libraryDependencies ++= Seq(
  "org.webjars.bower" % "bootstrap"                  % "4.0.0",
  "org.webjars.bower" % "tether"                     % "1.4.3",
  "org.webjars.bower" % "popper.js"                  % "1.12.9",
  "org.webjars.bower" % "angular-ui-bootstrap-bower" % "2.5.0",
  "org.webjars.bower" % "angular"                    % "1.6.9",
  "org.webjars.bower" % "metisMenu"                  % "2.5.0",
  "org.webjars.bower" % "OpenLayers"                 % "3.20.1",
  "org.webjars.bower" % "angular-google-chart"       % "0.1.0"
)

